package com.luna.random;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Random;

import android.text.TextUtils;

public class NumberRandom {
	public boolean isNotNullAndisNumber(String number) {
		number = number.trim();
		if (!TextUtils.isEmpty(number)) {
			return TextUtils.isDigitsOnly(number);
		}
		return false;
	}

	public boolean isNumber1LargerOrEqualsNumber2(String number1, String number2) {
		if (number1.equals(number2)) {
			return true;
		}
		BigInteger big1 = getNumber(number1);
		BigInteger big2 = getNumber(number2);
		if (big1.compareTo(big2) == 1) {
			return true;
		}
		return false;
	}

	public boolean checkRangeRight(String number1, boolean checked1,
			String number2, boolean checked2) {
		BigInteger big1 = getNumber(number1);
		BigInteger big2 = getNumber(number2);
		if (!checked1) {
			big1 = big1.add(new BigInteger("1"));
		}
		if (!checked2) {
			big2 = big2.subtract(new BigInteger("1"));
		}

		return !isNumber1LargerOrEqualsNumber2(big1.toString(), big2.toString());
	}

	public BigInteger getNumber(String numberString) {
		BigInteger return_number = new BigInteger(numberString, 10);
		return return_number;
	}

	public String randomNumber(String startNumber, boolean includeStart,
			String endNumber, boolean includeEnd) {
		BigInteger start = getNumber(startNumber);
		BigInteger end = getNumber(endNumber).add(new BigInteger("1"));
		if (!includeStart) {
			start = start.add(new BigInteger("1"));
		}
		if (!includeEnd) {
			end = end.subtract(new BigInteger("1"));
		}
		BigInteger random_range = end.subtract(start);

		double random_double = (new Random()).nextDouble();
		BigDecimal random_double_big = new BigDecimal(random_double);
		BigDecimal range_double_big = new BigDecimal(random_range);
		BigDecimal range_result_dec = random_double_big
				.multiply(range_double_big);
		BigInteger range_result_int = range_result_dec.toBigInteger();
		BigInteger random_result_int = range_result_int.add(start);

		String return_number_string = random_result_int.toString();
		return return_number_string;
	}
}
