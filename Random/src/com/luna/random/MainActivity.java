package com.luna.random;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Context;

public class MainActivity extends SherlockActivity implements
		ActionBar.OnNavigationListener {

	private int coin_item_count = 0;
	
	private RelativeLayout page1 = null;
	private RelativeLayout page2 = null;
	private RelativeLayout page3 = null;
	private RelativeLayout page4 = null;
	private RelativeLayout page5 = null;

	private Button coin_random_button = null;
	private Button coin_redo_button = null;
	private Button number_random_button = null;
	private Button number_redo_button = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		page1 = (RelativeLayout) findViewById(R.id.page_1);
		page2 = (RelativeLayout) findViewById(R.id.page_2);
		page3 = (RelativeLayout) findViewById(R.id.page_3);
		page4 = (RelativeLayout) findViewById(R.id.page_4);
		page5 = (RelativeLayout) findViewById(R.id.page_5);

		coin_random_button = (Button) findViewById(R.id.button_coin_random);
		coin_redo_button = (Button) findViewById(R.id.button_coin_redo);
		number_random_button = (Button) findViewById(R.id.button_number_ranodm);
		number_redo_button = (Button) findViewById(R.id.button_number_redo);

		coin_random_button.setOnClickListener(coinOnClickListener);
		coin_redo_button.setOnClickListener(coinOnClickListener);
		number_random_button.setOnClickListener(numberOnClickListener);
		number_redo_button.setOnClickListener(numberOnClickListener);

		Context context = getSupportActionBar().getThemedContext();
		ArrayAdapter<CharSequence> list = ArrayAdapter.createFromResource(
				context, R.array.locations, R.layout.sherlock_spinner_item);
		list.setDropDownViewResource(R.layout.sherlock_spinner_dropdown_item);

		getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		getSupportActionBar().setListNavigationCallbacks(list, this);

	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		page1.setVisibility(View.GONE);
		page2.setVisibility(View.GONE);
		page3.setVisibility(View.GONE);
		page4.setVisibility(View.GONE);
		page5.setVisibility(View.GONE);

		switch (itemPosition) {
		case 0:
			page1.setVisibility(View.VISIBLE);
			break;
		case 1:
			page2.setVisibility(View.VISIBLE);
			break;
		case 2:
			page3.setVisibility(View.VISIBLE);
			break;
		case 3:
			page4.setVisibility(View.VISIBLE);
			break;
		case 4:
			page5.setVisibility(View.VISIBLE);
			break;
		}

		return false;
	}

	OnClickListener coinOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.button_coin_random:
				coin_random();
				break;
			case R.id.button_coin_redo:
				coin_redo();
				break;
			}
		}

	};

	OnClickListener numberOnClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.button_number_ranodm:
				number_random();
				break;
			case R.id.button_number_redo:
				number_redo();
				break;
			}
		}

	};

	private void coin_random() {
		CoinRandom coin = new CoinRandom();
		boolean coin_face_boolean = coin.randomCoin();
		String coin_face_string = coin_face_boolean ? getString(R.string.coin_face_up)
				: getString(R.string.coin_face_back);
		String coin_face_string_short = coin_face_boolean ? getString(R.string.coin_face_up_short)
				: getString(R.string.coin_face_back_short);
		TextView COIN_FACE = (TextView) findViewById(R.id.textView_coin_face);
		COIN_FACE.setText(coin_face_string);
		LinearLayout COIN_RESULT = (LinearLayout) findViewById(R.id.coin_result_list);
		coin_item_count++;
		String item_string = getString(R.string.coin_times_text);
		item_string = item_string.replace("{0}", coin_item_count + "");
		TextView item = new TextView(this);
		item.setText(item_string + coin_face_string_short);
		COIN_RESULT.addView(item);
	}

	private void coin_redo() {
		((TextView) findViewById(R.id.textView_coin_face))
				.setText(getString(R.string.coin_face_default));
		coin_item_count = 0;
		((LinearLayout) findViewById(R.id.coin_result_list)).removeAllViews();
	}

	private void number_random() {

		EditText START_NUMBER = (EditText) findViewById(R.id.editText_number_start);
		EditText END_NUMBER = (EditText) findViewById(R.id.editText_number_end);
		CheckBox START_INCLUDE = (CheckBox) findViewById(R.id.checkBox_start_include);
		CheckBox END_INCLUDE = (CheckBox) findViewById(R.id.checkBox_end_include);
		TextView RANDOM_NUMBER = (TextView) findViewById(R.id.textView_random_number);
		String start_number_string = START_NUMBER.getText().toString().trim();
		String end_number_string = END_NUMBER.getText().toString().trim();
		NumberRandom random = new NumberRandom();
		if (!random.isNotNullAndisNumber(start_number_string)) {
			START_NUMBER.setFocusableInTouchMode(true);
			START_NUMBER.setSelected(true);
			START_NUMBER.requestFocus();
			Toast.makeText(
					this,
					getString(R.string.number_start_label)
							+ getString(R.string.number_not_number_text),
					Toast.LENGTH_LONG).show();
			return;
		}
		if (!random.isNotNullAndisNumber(end_number_string)) {
			END_NUMBER.setSelected(true);
			END_NUMBER.setFocusable(true);
			END_NUMBER.requestFocus();
			Toast.makeText(
					this,
					getString(R.string.number_end_label)
							+ getString(R.string.number_not_number_text),
					Toast.LENGTH_LONG).show();
			return;
		}
		if (random.isNumber1LargerOrEqualsNumber2(start_number_string,
				end_number_string)) {
			END_NUMBER.setSelected(true);
			END_NUMBER.setFocusable(true);
			END_NUMBER.requestFocus();
			Toast.makeText(this,
					getString(R.string.number_start_large_end_text),
					Toast.LENGTH_LONG).show();
			return;
		}
		if (!random.checkRangeRight(start_number_string,
				START_INCLUDE.isChecked(), end_number_string,
				END_INCLUDE.isChecked())) {
			END_NUMBER.setSelected(true);
			END_NUMBER.setFocusable(true);
			END_NUMBER.requestFocus();
			Toast.makeText(this, getString(R.string.number_no_range),
					Toast.LENGTH_LONG).show();
			return;
		}
		String random_number_string = random.randomNumber(start_number_string,
				START_INCLUDE.isChecked(), end_number_string,
				END_INCLUDE.isChecked());
		RANDOM_NUMBER.setText(random_number_string);
	}

	private void number_redo() {
		EditText START_NUMBER = (EditText) findViewById(R.id.editText_number_start);
		EditText END_NUMBER = (EditText) findViewById(R.id.editText_number_end);
		TextView RANDOM = (TextView) findViewById(R.id.textView_random_number);
		START_NUMBER.setText(getString(R.string.number_start_number_auto));
		END_NUMBER.setText(getString(R.string.number_end_number_auto));
		RANDOM.setText(R.string.number_random_number);
	}
}
